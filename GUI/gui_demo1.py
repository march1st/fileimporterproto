"""
Simple example with a basic GUI to start with
"""

import tkinter as tk


class GUI(tk.Tk):
    """
       Simple GUI window constructor
    """
    def __init__(self):
        tk.Tk.__init__(self)
        self.title('App window title')
        self.frame = tk.Frame(self, width=600, height=250)
        #self.button = self.frame.Button(self, text="Click", )
        self.frame.pack()
        self.button = tk.Button(self, cnf={
                "name":"ok",
                "text":"OK",
                "bg": "green",
                "fg": "red",
            "highlightcolor": "blue"
            }, command=self.change_color('black')).pack()

    def read_options(self, json_file):
        with open(json_file) as f:
            for k, v in json_file:
                print(k, v)


    def show_options(self, file):
       pass

    def change_color(self, color):
        print(color)
        self.frame.configure(background='blue')

    def quit(self):
        self.destroy()



g = GUI()
g.mainloop()