from _datetime import datetime as dt


class Converter:

    def __init__(self, filename):
        self.filename = filename

    # read file and turn upside down (TODO: type return see link at bottom)
    def read_file(self, filename):
        """
        :rtype: List
        """
        lines = None
        try:
            with open(filename) as f_name:
                lines = f_name.readlines()
        except FileNotFoundError:
            print('File %s not found', filename)
        return lines

    def check_spacing(self):
        curr = dt.now().strftime('%Y%m%d%H%M00')
        print(curr + str(type(curr)))
        # for comparison of formatting
        # TODO: use pandas to CREATE timestamp spacing
        print(20160624090000)
        spacing = 10
        return spacing

    def convert_timestamps(self):
        # see https://pandas.pydata.org/pandas-docs/stable/timeseries.html TODO
        now = dt.now().strftime('%Y%m%d%H%M00')


# https://docs.python.org/3/library/typing.html