import pathlib
import unittest
import file_io.converter as cv
import os

from datetime import datetime


class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.c = cv.Converter('../../data/testdata.zrx')

    def test_something(self):
        self.assertEqual(True, True)

    def test_constructor(self):
        self.c.check_spacing()
        self.assertIsNotNone(self.c)

# https://dbader.org/blog/python-check-if-file-exists
    def test_read_file(self, path=None):
        timestamps = []
        datetimes = []
        new_dates = []
        path = path or pathlib.Path('../../data/testdata.zrx')
        f = self.c.read_file(path)
        print(type(f))
        i = 0
        for line in f:
            if (line[0] != '#'):
                timestamps.append(line.split(' ', 1)[0])
                datetimes.append(datetime.strptime((timestamps[i]), '%Y%m%d%H%M00'))
                i += 1
        delta = (datetimes[1] - datetimes[0])
        print('DELTA: ' + delta.__str__())
        curr = datetime.now()
        print('NOW: ' + curr.__str__())
        for x in range(len(datetimes)):
            new_dates.append(curr - delta)
            curr -= delta
        new_dates.reverse()
        for d in new_dates:
            print(datetime.strftime(d, '%Y%m%d%H%M00'))
        self.assertTrue(f, os.path.isfile(path))
        self.assertIs(type(datetimes[0]), datetime, 'Yes!')


if __name__ == '__main__':
    unittest.main()
